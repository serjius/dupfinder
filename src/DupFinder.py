#!/usr/bin/python

import os
import sys
import hashlib
import sqlite3 as sql
import multiprocessing

SQL_PREP_DUPS="""
INSERT INTO FilesDups (FilePath, FileSize, FileCheckSumm, ShareName)
  SELECT FilePath, FileSize, FileCheckSumm, ShareName
  FROM Files
  WHERE FileSize in (
    SELECT FileSize
    FROM Files
    GROUP BY Filesize HAVING count(*)>1
  ) order by FileSize;
"""
# DB Files work
class FilesDBWork():
    dbname = None
    data = []

    def __new__(self):
        return self

    def __init__(self):
        self.lock = multiprocessing.Lock()
        self.dbname = 'files.sqlite3'
        self.conn = sql.connect(self.dbname)
        self.conn.text_factory = str
        self.cur = self.conn.cursor()
        self.cur.execute('CREATE TABLE IF NOT EXISTS Files (FileID INTEGER PRIMARY KEY AUTOINCREMENT, FilePath TEXT, FileSize bigint, FileCheckSumm TEXT, ShareName TEXT)')
        self.cur.execute('CREATE TABLE IF NOT EXISTS FilesDups (FileID INTEGER PRIMARY KEY AUTOINCREMENT, FilePath TEXT, FileSize bigint, FileCheckSumm TEXT, ShareName TEXT)')

    def addFile(self, fpath, fsize):
        self.data.append((fpath, fsize))

    def commit_trans(self):
        self.cur.executemany('INSERT INTO Files (FilePath, FileSize, FileCheckSumm) VALUES (?, ?, "-")', self.data)
        self.conn.commit()
        self.data = []

    def get_files_part(self, worker_num):
        self.lock.acquire()
        self.cur.execute("select * from FilesDups where FileCheckSumm ='-' limit 50;")
        data = self.cur.fetchall()
        id = []
        for i in data:
            id.append(i[0])
        placeholder= '?'
        placeholders = ', '.join(placeholder for unused in id)

        SQL = "update FilesDups set FileCheckSumm='%d' where FileCheckSumm not null and FileID in (%s)" % (worker_num, placeholders)
        self.cur.execute(SQL, id)
        self.conn.commit()
        self.lock.release()
        return data

    def update_hash(self, files):
        self.lock.acquire()
        for file in files:
            self.cur.execute('UPDATE FilesDups SET FileCheckSumm=? where FileID=?', (file[1], file[0]))
        self.conn.commit()
        self.lock.release()

    def prep_dup_find(self):
        self.cur.execute(SQL_PREP_DUPS)
        self.conn.commit()

    def close(self):
        self.cur.close()
        self.conn.close()


def findDup(parentFolder):
    # Dups in format {hash:[names]}
    FDB = FilesDBWork()
    dups = {}
    for dirName, subdirs, fileList in os.walk(parentFolder):
        for filename in fileList:            
            path = os.path.join(dirName, filename)
            if not os.path.isfile(path):
                continue
            FDB.addFile(path, os.path.getsize(path))
    FDB.commit_trans()
    FDB.close()
    return dups


def hashfile(path, blocksize = 65536):
    afile = open(path, 'rb')
    hasher = hashlib.md5()
    buf = afile.read(blocksize)
    while len(buf) > 0:
        hasher.update(buf)
        buf = afile.read(blocksize)
    afile.close()
    return hasher.hexdigest()


def worker(num, fdb):
    """worker function"""
    print('Worker %d' % num)
    data = fdb.get_files_part(num)
    res = []
    while data:
        for file in data:
            if os.path.exists(file[1]):
                hash = hashfile(file[1])
                res.append((file[0], hash))
        data = fdb.get_files_part(num)
        print('Worker %d next part' % num)
        fdb.update_hash(res)
    return


def printResults(dict1):
    results = list(filter(lambda x: len(x) > 1, dict1.values()))
    if len(results) > 0:
        print('Duplicates Found:')
        print('The following files are identical. The name could differ, but the content is identical')
        print('___________________')
        for result in results:
            for subresult in result:
                print('\t\t%s' % subresult)
            print('___________________')

    else:
        print('No duplicate files found.')


if __name__ == '__main__':
    
    if len(sys.argv) > 1:
        dups = {}
        folders = sys.argv[1:]
        for i in folders:
            if os.path.exists(i):
                findDup(i)
                pass
            else:
                print('%s is not a valid path, please verify' % i)
                sys.exit()
        jobs = []
        fdb = FilesDBWork()
        print('Prepare find duplicates')
        fdb.prep_dup_find()
        for i in range(10):
            p = multiprocessing.Process(target=worker, args=(i, fdb))
            jobs.append(p)
            p.start()
    else:
        print('Usage: python dupFinder.py folder or python dupFinder.py folder1 folder2 folder3')
